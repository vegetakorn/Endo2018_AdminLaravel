<html>
<head>
    <title>Constancia de participación</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <img src="https://www.kgrouptravel.com/wp-content/uploads/2017/12/banner.png" alt="KGroup Wellbeing Meetings">
    </div>
    <br/><br/>
    <div class="opps">
        <div class="opps-instructions">
            Estimado <strong>{{$nombre_completo}}</strong>
            <br />
            <br />
            Me permito proporcionarle la liga para que descargue su constancia del Curso de Actualizaci&oacute;n en Endocrinolog&iacute;a, que se llev&oacute; a cabo los d&iacute;as 18, 19 y 20 de enero 2018. <br /> <br />
                Le informo que este ser&aacute; el &uacute;nico medio y periodo de tiempo (23 de enero al 28 de febrero 2018) por el cual podr&aacute; obtener su constancia.
            <br />
            <br />
            <strong><a href="http://www.endocrinologia.myregistration.com.mx/admin/const/{{$tipo_dato}}/{{$id_persona}}/{{$cons_folio}}">Descargue dando click aquí</a></strong>
            <br />
            <br />
            Saludos!
            <BR />
        </div>
    </div>
</div>
</body>
</html>