@extends('admin_template')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Filtro de Búsqueda  </h3>
                    <a style="float: right"  href="{{route('registros.exportar')}}" class="btn btn-primary  btn-flat">Exportar información</a>
                    <!-- radio -->

                    <div class="form-group">
                        <form action="{{route('socios.filtro')}}" method="post">
                            {{csrf_field()}}
                            <div class="row" style="margin-left: 10px;">
                                <label>
                                    <input type="radio" value="0" name="r1" class="minimal" checked onclick="document.getElementById('cboTipo').disabled=true; ">
                                    Todos
                                </label>
                                <label>
                                    <input type="radio" value="1" name="r1" class="minimal" onclick="document.getElementById('cboTipo').disabled=false; ">
                                    Por Tipo

                                </label>
                                <label>
                                    <select id="cboTipo" name="existente"  class="form-control select2" style="width: 100%;" disabled>
                                        <option value="0"></option>
                                        <option value="1">Socios</option>
                                        <option value="2">Nuevo Registro</option>
                                    </select>
                                </label>
                            </div>
                            <div class="row" style="margin-left: 10px;">
                                <label>
                                    Por Nombre y/o Apellidos:

                                </label>
                                <label>
                                    <input class="form-control  input-sm" name="nombre" type="text" value="{{old('nombre')}}" placeholder="Nombre y/o Apellidos">

                                </label>
                                <label>
                                    <button type="submit"   class="btn btn-block btn-default btn-sm">Buscar</button>
                                </label>
                            </div>

                        </form>
                    </div>

                </div>

                <!-- /.box-header -->
                <div class="box-body">



                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Número de socio</th>
                            <th>Es Socio</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>E-Mail</th>
                            <th>Registrado</th>
                            <th>Fecha de Registro</th>

                            <th></th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($socios as $socio)
                            <tr>
                                <td width="10%"> {{ $socio->num_socio }}</td>
                                <td width="10%"> {{ ($socio->existente == 1)?"Socio":"Nuevo Registro" }}</td>
                                <td>{{ $socio->nombres2 }}</td>
                                <td>{{ $socio->apellidos2 }}</td>
                                <td width="10%">{{ $socio->mail }}</td>
                                <td>{{ $socio->submission_id }}</td>
                                <td>{{ $socio->fh_registro }}</td>
                                <td align="center"><a href="{{ route('socios.info', $socio->id) }}"   ><i class="fa fa-file-text"> </i> Ver Más</a ></td>
                                <td align="center"><a href="{{ route('socios.anualidades', $socio->id) }}"   ><i class="fa fa-file-text"> </i> Anualidades</a ></td>
                            </tr>

                        @endforeach


                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row  <a ><i class="fa fa-fw fa-trash"></i> Borrar</a> -->
        <script>

            function enviar() {
                submit();
                return false
            };
        </script>
    </section>
@endsection