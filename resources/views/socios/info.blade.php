@extends('admin_template')

@section('content')
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">General</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>

                </div>
            </div>
            <!-- form start -->
            <form class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Es Socio</label>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" value="<?php if (isset($socio)) { if($socio['existente'] == 1){ echo "Socio";}else{  echo "Nuevo Registro";}}else{echo "";}; ?>" disabled >
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Número de Socio</label>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" value="<?php if (isset($socio)) {echo $socio['num_socio'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Año Ingreso</label>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" value="<?php if (isset($socio)) {echo $socio['anio_inicio'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Nombre(s)</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php if (isset($socio)) {echo $socio['nombres2'];}else{echo "";}; ?>" disabled >
                        </div>

                    </div>
                    <div class="form-group">

                        <label  class="col-sm-2 control-label">Apellidos</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php if (isset($socio)) {echo $socio['apellidos2'];}else{echo "";}; ?>" disabled  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Fecha de Nacimiento</label>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" value="<?php if (isset($socio)) {echo $socio['fh_ingreso'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>
                    <div class="form-group">

                        <label  class="col-sm-2 control-label">Residencia</label>

                        <div class="col-sm-2">
                            <input type="textarea" class="form-control" value="<?php if (isset($socio)) {echo $socio['residencia'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>
                    <div class="form-group">

                        <label  class="col-sm-2 control-label">E-Mail 1</label>

                        <div class="col-sm-5">
                            <input type="textarea" class="form-control" value="<?php if (isset($socio)) {echo $socio['mail'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>
                    <div class="form-group">

                        <label  class="col-sm-2 control-label">E-Mail 2</label>

                        <div class="col-sm-5">
                            <input type="textarea" class="form-control" value="<?php if (isset($socio)) {echo $socio['mail2'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <!--<button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Registro</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Submission ID</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control"  value="<?php if (isset($registro)) {echo $registro['submission_id'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Fecha de Ingreso</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php if (isset($registro)) {echo $registro['fh_registro'];}else{echo "";}; ?>" disabled >
                        </div>
                    </div>

                    @if($socio['datos_adicionales'] != "")
                        <div class="form-group">

                            <?php $datos = json_decode($socio['datos_adicionales']);?>

                            <label  class="col-sm-2 control-label">Datos Adicionales  </label>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Id único </label>
                            <div class="col-sm-5">
                                <input type="textarea" class="form-control" value="<?php print_r($datos->q58_uniqueid );?>" disabled >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Dirección</label>
                            <div class="col-sm-5">
                                <input type="textarea" class="form-control" value="<?php print_r($datos->q6_direccionDe->addr_line1 ); print_r($datos->q6_direccionDe->addr_line2 ); print_r($datos->q6_direccionDe->city );?>" disabled >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Tipo</label>
                            <div class="col-sm-5">
                                <input type="textarea" class="form-control" value="<?php print_r($datos->q60_tipoDe );?>" disabled >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Requiere factura</label>
                            <div class="col-sm-5">
                                <input type="textarea" class="form-control" value="<?php print_r($datos->q46_requiereFactura );?>" disabled >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Razón social</label>
                            <div class="col-sm-5">
                                <input type="textarea" class="form-control" value="<?php print_r($datos->q11_razonSocial );?>" disabled >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">RFC</label>
                            <div class="col-sm-5">
                                <input type="textarea" class="form-control" value="<?php print_r($datos->q10_rfc );?>" disabled >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Dirección facturación  </label>
                            <div class="col-sm-5">
                                <input type="textarea" class="form-control" value="<?php print_r($datos->q12_direccion->addr_line1 );print_r($datos->q12_direccion->addr_line2 );?>" disabled >
                            </div>
                        </div>
                    @endif

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <!--<button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Pagos</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Tipo de Pago</th>
                        <th>Fecha</th>
                        <th>$ Total</th>
                        <th>$ Curso</th>
                        <th>Estatus</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pagos as $pago)
                        <?php
                            $tipo_pago = "";
                            switch($pago->tipo_pago)
                            {
                                case 1:
                                    $tipo_pago = "Tarjeta de Crédito";
                                    break;
                                case 2:
                                    $tipo_pago = "Depósito";
                                    break;
                                case 3:
                                    $tipo_pago = "Transferencia";
                                    break;
                                case 4:
                                    $tipo_pago = "Sin monto a pagar";
                                    break;
                                case 5:
                                    $tipo_pago = "Depósito o transferencia";
                                    break;
                                default:
                                    $tipo_pago = "Sin definir";
                                    break;

                            }

                        ?>
                        <tr>
                            <td > {{ $tipo_pago}}</td>
                            <td > {{ $pago->fh_transaccion }}</td>
                            <td > {{ $pago->monto_pago_total }}</td>
                            <td > {{ $pago->monto_pago_curso }}</td>
                            <td>{{ $pago->estatus_pago }}</td>
                            @if($pago->estatus_pago != 'Pagado')
                                <td align="center"><a href="{{ route('socios.pago',[$socio['id'], $socio['registros_id'], $pago->id]) }}"   ><i class="fa fa-file-text"> </i>Pagar</a ></td>
                            @else
                                <td>&nbsp;</td>
                            @endif
                        </tr>

                    @endforeach


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <a href="{{route('registros.exportar')}}" class="btn btn-primary  btn-flat" target="_blank">Generar nuevo pago (TC)</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="{{route('registros.exportar')}}" class="btn btn-primary  btn-flat" target="_blank">Generar nuevo pago (Depósito)</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="{{route('registros.exportar')}}" class="btn btn-primary  btn-flat" target="_blank">Generar nuevo pago (Transferencia)</a>
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </section>
@endsection
