@extends('admin_template')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"> </h3>
                </div>
                <?php


                ?>

                <!-- /.box-header -->
                <div class="box-body">



                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Año</th>
                            <th>Monto</th>
                            <th>Pagado</th>
                            <th>Status</th>
                            <th>Categorías</th>
                            <th>Otro</th>
                            <th>Fecha de Pago</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($anualidades as $anualidad)
                            <?php
                            $st_pago = "";
                            switch($anualidad->st_pago)
                            {
                                case 1:
                                    $st_pago = "Pendiente";
                                    break;
                                case 2:
                                    $st_pago = "Pagado";
                                    break;
                                case 3:
                                    $st_pago = "Exento";
                                    break;

                                default:
                                    $st_pago = "Sin definir";
                                    break;

                            }

                            ?>
                            <tr>
                                <td width="10%"> {{ $anualidad->anio }}</td>
                                <td width="10%">{{ $anualidad->monto }} </td>
                                <td>{{ $anualidad->pagado }}</td>
                                <td>{{ $st_pago }}</td>
                                <td width="10%">{{ $anualidad->nombre }}</td>
                                <td>{{ $anualidad->otro }}</td>
                                <td>{{ $anualidad->fh_pago }}</td>
                                <td>

                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default" >
                                            Pagar
                                        </button>

                                </td>

                            </tr>


                        @endforeach


                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>

        <!-- /.row  <a ><i class="fa fa-fw fa-trash"></i> Borrar</a> -->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Pagar {{$id}}</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            {{csrf_field()}}
                            <input type="hidden" class="form-control" value="" name="id" >
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Motivo de Pago</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="" name="clave" placeholder="Descripción o motivo que captura el usuario">
                                    </div>
                                </div>


                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    </section>
@endsection