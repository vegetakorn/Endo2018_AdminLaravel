@extends('admin_template')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Especialidad  </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{route('formespecialidad')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" value="<?php if (isset($especialidad)) {echo $especialidad['id'];}else{echo 0;}; ?>" name="id" >
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Clave</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if (isset($especialidad)) {echo $especialidad['clave'];}; ?>" name="clave" placeholder="Clave de la especialidad">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Nombre</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if (isset($especialidad)) {echo $especialidad['nombre'];}; ?>" name="nombre" placeholder="Nombre de la especialidad">
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="/especialidades" class="btn btn-default pull-right">Cancelar</a>
                        <button type="submit" class="btn btn-info pull-right"><?php if (isset($especialidad)) {echo "Actualizar";}else{echo "Agregar";}; ?></button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection