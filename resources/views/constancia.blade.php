<!-- Formulario aquí -->

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Generacion de Constancia PDF</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <a id="genPDF" class="btn btn-success" href="javascript: submitDetailsForm()" >Crear Constancias</a>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Folio</th>
                    <th>Nombre</th>

                    <th>Participación</th>

                </tr>
                </thead>
                <tbody>
            @foreach($asistentes as $asistente)
                <form id="form{{$asistente->folio}}" action="{{route('genpdfimg')}}" method="POST" >
                    {{ csrf_field() }}
                    <tr>
                        <td> <input type="text" class="form-control" name="folio"  value="{{ $asistente->folio }}"  disabled /></td>
                        <td><input type="text" class="form-control" name="nombre"  value="{{ $asistente->nombre }}"  disabled /></td>
                        <td><input type="text" class="form-control" name="participacion"  value="{{ $asistente->participacion }}"  disabled /></td>

                    </tr>
                </form>


            @endforeach
                </tbody>

            </table>



        </div>
    </div>
</div>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript">

    function submitDetailsForm() {



        var form = document.createElement("form");
            $(form).attr("action", "{{route('genpdfimg')}}")
                .attr("method", "post");

            $(form).html('<input type="hidden"  name="asistentes"  value="{{ $asistentes }}"   />  />');

            document.body.appendChild(form);
            $(form).submit();




    }

</script>
</body>
</html>
