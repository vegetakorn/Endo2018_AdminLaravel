@extends('admin_template')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Listado de Especialidades  </h3>
                        <a style="float: right"  href="formespecialidad" class="btn btn-primary  btn-flat">Crear Especialidad</a>

                    </div>



                    <!-- /.box-header -->
                    <div class="box-body">



                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Clave</th>
                                <th>Especialidad</th>

                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($especialidades as $especialidad)
                                <tr>
                                    <td> {{ $especialidad['clave'] }}</td>
                                    <td>{{ $especialidad['nombre'] }}</td>
                                    <td align="center"><a href="formespecialidad/{{$especialidad['id']}}"   ><i class="fa fa-edit"> </i> Editar</a ></td>

                                </tr>

                            @endforeach


                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Clave</th>
                                <th>Especialidad</th>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row  <a ><i class="fa fa-fw fa-trash"></i> Borrar</a> -->
    </section>
@endsection