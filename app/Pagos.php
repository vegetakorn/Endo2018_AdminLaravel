<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Pagos extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'pagos';

    protected $fillable = [
        'asistentes_id', 'cursos_id', 'fh_transaccion', 'tipo_pago', 'fh_vigencia',
        'result_op', 'result_datos', 'customer_data', 'order_data', 'error_data',
        'customer_result', 'order_result','monto_pago_total','monto_pago_curso','estatus_pago',
        'order_result2', 'order_id',
    ];
}
