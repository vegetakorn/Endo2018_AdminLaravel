<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Especialidad extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'especialidades';

    protected $fillable = [
        'clave', 'nombre',
    ];


}
