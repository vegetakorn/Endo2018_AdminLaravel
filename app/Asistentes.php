<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Asistentes extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'asistentes';

    protected $fillable = [
        'registros_id', 'existente', 'mail', 'mail2', 'num_socio',
        'fh_ingreso', 'residencia', 'nombre', 'datos_adicionales', 'anio_inicio',
        'nombres2', 'apellidos2',
    ];



}
