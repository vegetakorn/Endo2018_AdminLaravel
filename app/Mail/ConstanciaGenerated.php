<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;


class ConstanciaGenerated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $const_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($const_data)
    {
        $this->const_data = $const_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('payments2@myregistration.com.mx', 'SMNE')
            ->subject ('Curso de Actualización SMNE (Folio: ' . $this->const_data['cons_folio'] . ')')
            ->view('emails.constancia.generated', $this->const_data );
    }
}
