<?php

namespace App\Http\Controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ExportarController extends Controller
{
    public function generar()
    {
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $spreadsheet->getProperties()->setCreator('Mario Galicia')
            ->setLastModifiedBy('Mario Galicia')
            ->setTitle('SMNE')
            ->setSubject('SMNE')
            ->setDescription('Información de registros')
            ->setKeywords('office 2007 openxml ');

        //Acceso a la primer hoja
        $sheet = $spreadsheet->getActiveSheet();

        //Carga Encabezados
        $sheet->setCellValue('A1', 'Número Socio');
        $sheet->setCellValue('B1', 'E-mail');
        $sheet->setCellValue('C1', 'Nombre(s)');
        $sheet->setCellValue('D1', 'Apellido(s)');
        $sheet->setCellValue('E1', 'Tipo Socio');
        $sheet->setCellValue('F1', 'Id - Registro');
        $sheet->setCellValue('G1', 'Fecha Registro');
        $sheet->setCellValue('H1', 'Forma de Pago');
        $sheet->setCellValue('I1', 'Fecha Transacción');
        $sheet->setCellValue('J1', 'Resultado Operacion');
        $sheet->setCellValue('K1', 'Monto Total');
        $sheet->setCellValue('L1', 'Monto Curso');
        $sheet->setCellValue('M1', 'Estatus del Pago');

        $sheet->getStyle('A1:M1')->getFont()->setBold(true);

        //Obtiene información a exportar
        $registros = DB::table('vwAsistentesRegistros')->get();
        $NumRow = 1;

        foreach ($registros as $data)
        {
            $NumRow++;
            //Carga registro por registro
            $sheet->setCellValue('A' . $NumRow, $data->num_socio);
            $sheet->setCellValue('B' . $NumRow, $data->mail);
            $sheet->setCellValue('C' . $NumRow, $data->Nombre);
            $sheet->setCellValue('D' . $NumRow, $data->Apellido);
            $sheet->setCellValue('E' . $NumRow, $data->TipoSocio);
            $sheet->setCellValue('F' . $NumRow, $data->submission_id . ' ');
            if ($data->fh_registro=="")
            {
                $sheet->setCellValue('G' . $NumRow, '');

            } else
            {
                $fh_registro_time = strtotime($data->fh_registro);
                $sheet->setCellValue('G' . $NumRow, date("d/m/y g:i A", $fh_registro_time));
                $sheet->getStyle('G' . $NumRow)
                    ->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DATETIME);

            }
            //$sheet->setCellValue('G' . $NumRow, '');
            $sheet->setCellValue('H' . $NumRow, ($data->submission_id=="")?"":$data->FormaPago);
            if ($data->fh_transaccion=="")
            {
                $sheet->setCellValue('I' . $NumRow, '');

            } else
            {
                $fh_registro_time = strtotime($data->fh_transaccion);
                $sheet->setCellValue('I' . $NumRow, date("d/m/y g:i A", $fh_registro_time));
                $sheet->getStyle('I' . $NumRow)
                    ->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DATETIME);

            }
            $sheet->setCellValue('J' . $NumRow, $data->result_op);
            $sheet->setCellValue('K' . $NumRow, $data->monto_pago_total);
            $sheet->setCellValue('L' . $NumRow, $data->monto_pago_curso);
            $sheet->setCellValue('M' . $NumRow, $data->estatus_pago);
        }

        /*
        $writer = new Xlsx($spreadsheet);
        $writer->save('ExportarRegistro.xlsx');
        */

        //Crea un autofilter
        $sheet->setAutoFilter($sheet->calculateWorksheetDimension());

// Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="ExportarRegistro.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');


    }
}
