<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistentes;
use App\Registros;
use App\Pagos;
use DB;
class AsistentesController extends Controller
{
    public function index() {



        $data['socios'] = DB::table('asistentes')
                            ->leftjoin('registros', function ($join) {
                                $join->on('registros.id', '=', 'asistentes.registros_id');
                            })
                            ->select('asistentes.*', 'registros.submission_id', 'registros.fh_registro')
                            ->orderBy('asistentes.nombres2','asc')->orderBy('asistentes.apellidos2','asc')
                            ->get();
        /*$data['socios'] = DB::table('asistentes')
                            ->whereNull('registros_id')
                            ->get();*/


        $data['titulo'] = 'Registro de Socios';
        $data['descripcion'] = 'Muestra el listado de socios y sus respectivos registros';


        return view('socios/registros')->with( $data);
    }

    public function filter(Request $req) {
        //dd($req->all());

         $sql = DB::table('asistentes');
         $sql->leftjoin('registros', function ($join) {
                $join->on('registros.id', '=', 'asistentes.registros_id');
            });

        if($req->input('existente') != 0)
        {
            $sql->where('asistentes.existente','=',$req->input('existente'));
        }

        if($req->input('nombre') != '')
        {
            $sql->where(DB::raw("CONCAT(`nombres2`, ' ', `apellidos2`)"),'LIKE','%'.$req->input('nombre').'%');
        }

        $sql->orderBy('asistentes.nombres2','asc')->orderBy('asistentes.apellidos2','asc');
        $data['socios'] = $sql->get();
        $data['titulo'] = 'Registro de Socios';
        $data['descripcion'] = 'Muestra el listado de socios y sus respectivos registros';


        return view('socios/registros')->with( $data);
    }


    public function show($id) {


        $data['socio']  = Asistentes::find($id);
        $data['registro']  = Registros::find($data['socio']['registros_id']);

        $sql = DB::table('pagos');
        $sql->where('pagos.asistentes_id','=',$data['socio']['id']);
        $data['pagos'] = $sql->get();

        $data['titulo'] = 'Información del Socio';
        $data['descripcion'] = 'Información General, Registro y Pagos';


        return view('socios/info')->with( $data);
    }

    public function showpago($idasis, $idreg, $idpago) {

        $data['idasistente'] = $idasis;
        $data['idregistro'] = $idreg;
        $data['idpago'] = $idpago;
        $data['titulo'] = 'Pagar';
        $data['descripcion'] = '';

        return view('socios/pago')->with( $data);
    }

    public function showanualidad($id) {

        $sql = DB::table('anualidades');
        $sql->leftjoin('categorias', function ($join) {
            $join->on('categorias.id', '=', 'anualidades.categorias_id');
        })->select('anualidades.*', 'categorias.clave', 'categorias.nombre');
        $sql->where('anualidades.asistentes_id','=',$id);

        $data['anualidades'] = $sql->get();

        $data['titulo'] = 'Información de Anualidades';
        $data['descripcion'] = '';


        return view('socios/anualidades')->with( $data);
    }
}
