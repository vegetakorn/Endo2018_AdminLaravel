<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Especialidad;

class EspecialidadController extends Controller
{
    public function index() {



        $data['especialidades'] = Especialidad::get();
        $data['titulo'] = 'Catálogo de Especialidades';
        $data['descripcion'] = 'Muestra el listado de especialidades para su creación, edición y borrado';


        return view('especialidades')->with( $data);
    }

    public function create() {

        $data['titulo'] = 'Crear Especialidad';
        $data['descripcion'] = 'Llena la información para crear una especialidad';


        return view('formespecialidad')->with( $data);
    }

    public function store(Request $request)
    {
    //$this->validation($request);

        if($request->input('id') != 0)
        {
            Especialidad::find($request->input('id'))->update($request->all());
        }else
        {
            Especialidad::create(
                array("clave" => $request->input('clave'),
                    "nombre" => $request->input('nombre'))
            );
        }


        //return $request->all();
        return redirect('/especialidades');
    }

    public function edit( $id)
    {

        $data['especialidad']  = Especialidad::find($id);

        $data['titulo'] = 'Editar Especialidad';
        $data['descripcion'] = 'Cambia información de la especialidad';

        return view('formespecialidad')->with( $data);

    }

    public function update(Request $request, $id)
    {

        Especialidad::find($id)->update($request->all());

        return redirect('/especialidades');

    }


}
