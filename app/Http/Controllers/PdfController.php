<?php

namespace App\Http\Controllers;

use Dompdf\Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use App\Mail\ConstanciaGenerated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;


class PdfController extends Controller
{
    public function index()
    {
        $sql = DB::table('vwConstancias');

        $data['asistentes'] = $sql->get();

        return view('constancia')->with( $data);;
    }

    public function downloadPDF(Request $info)
    {
        // dd($info->all());
        //  $info = array($request);

        $pdf = PDF::loadView('pdfView', compact('info'));
        $pdf->setPaper([0, 0, 600.98, 800.85], 'landscape');

        return $pdf->download('constancia.pdf');
    }

    public function downloadPDFImg(Request $data)
    {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        //dd($info->all());
        $arrAsis = json_decode($data->input('asistentes'));
        foreach($arrAsis as $key=>$value){
            $folio = $value->folio;
            $nombre = explode(" ", $value->nombre);
            $nombreArchivo =  $folio."_";
            $count = count($nombre) - 1;
            for ($i=0; $i < count($nombre);$i++)
            {
                if($count == $i)
                {
                    $nombreArchivo .= $nombre[$i].".pdf";
                }else
                {
                    $nombreArchivo .= $nombre[$i]."_";
                }

            }
            $info['folio'] = $value->folio;
            $info['nombre'] = $value->nombre;
            $info['participacion'] = $value->participacion;

            //Se modifica el nombre del archivo
            $nombreArchivo2 = utf8_decode($nombreArchivo);
            $nombreArchivo2 = strtr($nombreArchivo2, utf8_decode($originales), $modificadas);
            $nombreArchivo2 = strtoupper($nombreArchivo2);
            $nombreArchivo2 = utf8_encode($nombreArchivo2);


            //$pdf = PDF::loadView('pdfViewImg', compact('info'));
            $pdf = PDF::loadView( 'pdfViewImg', compact('info'));
            $pdf->setPaper([0, 0, 600.98, 800.85], 'landscape');
            $pdf->save( 'Constancias/'. $nombreArchivo2 );

            //Actualiza el nombre del archivo en el registro

            if ($value->tipo == "a")
            {
                DB::table('asistentes')
                    ->where('cons_folio', $value->folio)
                    ->update([
                        'cons_archivo' => $nombreArchivo2
                    ]);
            }
            else if ($value->tipo == "p")
            {
                DB::table('ponentes')
                    ->where('cons_folio', $value->folio)
                    ->update([
                        'cons_archivo' => $nombreArchivo2
                    ]);
            }

           // dd($value->nombre);
        }

        //  $info = array($request);
       /* */
        //return $pdf->download('constanciaImg.pdf');

    }

    public function crarConstanciaId(Request $data, $folio)
    {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        //dd($info->all());
        $datos_constancia = DB::table('vwConstancias')->where('folio', '=', $folio)->first();

        $folio = $datos_constancia->folio;
        $nombre = explode(" ", $datos_constancia->nombre);
        $nombreArchivo =  $folio."_";
        $count = count($nombre) - 1;
        for ($i=0; $i < count($nombre);$i++)
        {
            if($count == $i)
            {
                $nombreArchivo .= $nombre[$i].".pdf";
            }else
            {
                $nombreArchivo .= $nombre[$i]."_";
            }

        }
        $info['folio'] = $datos_constancia->folio;
        $info['nombre'] = $datos_constancia->nombre;
        $info['participacion'] = $datos_constancia->participacion;

        //Se modifica el nombre del archivo
        $nombreArchivo2 = utf8_decode($nombreArchivo);
        $nombreArchivo2 = strtr($nombreArchivo2, utf8_decode($originales), $modificadas);
        $nombreArchivo2 = strtoupper($nombreArchivo2);
        $nombreArchivo2 = utf8_encode($nombreArchivo2);


        //$pdf = PDF::loadView('pdfViewImg', compact('info'));
        $pdf = PDF::loadView( 'pdfViewImg', compact('info'));
        $pdf->setPaper([0, 0, 600.98, 800.85], 'landscape');
        $pdf->save( 'Constancias/'. $nombreArchivo2 );

        //Actualiza el nombre del archivo en el registro

        if ($datos_constancia->tipo == "a")
        {
            DB::table('asistentes')
                ->where('cons_folio', $datos_constancia->folio)
                ->update([
                    'cons_archivo' => $nombreArchivo2
                ]);
        }
        else if ($datos_constancia->tipo == "p")
        {
            DB::table('ponentes')
                ->where('cons_folio', $datos_constancia->folio)
                ->update([
                    'cons_archivo' => $nombreArchivo2
                ]);
        }

        //  $info = array($request);
        /* */
        //return $pdf->download('constanciaImg.pdf');

    }
    /**
     * Permite la descarga de archivos PDF.
     * @param Request $request
     * @param $tipo
     * @param $id
     * @param $folio
     * @return mixed
     */
    public function descargaConstancia(Request $request, $tipo, $id, $folio)
    {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        //Buscar la constancia
        $constancia = null;
        if ($tipo=="a")
        {
            $constancia = DB::table('asistentes')->where('cons_folio','=',$folio)->where('id','=',$id)->first();
        } else if ($tipo=="p")
        {
            $constancia = DB::table('ponentes')->where('cons_folio','=',$folio)->where('id','=',$id)->first();
        }

        //Envía a otra URL en caso de no existir el PDF
        if ($constancia == null)
        {
            Log::info('Constancia no existente (null): ' . $tipo . "|" . $id . "|" . $folio . "-");
            return Redirect::to("https://www.kgrouptravel.com/error-constancia/");
        }

        //Se modifica el nombre del archivo
        $nombreArchivo2 = $constancia->cons_archivo;

        //Valida el nombre del archivo

        //PDF file is stored under project/public/constancias/*.pdf
        /*
        $file= public_path(). "/constancias/" . $nombreArchivo2 ;
        $file= "http://www.endocrinologia.myregistration.com.mx/admin/constancias/" . $nombreArchivo2;
        */
        $file= "/home4/c3g0x2w5/endocrinologia.myregistration.com.mx/admin/constancias/" . $nombreArchivo2;

        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$nombreArchivo2.'"'
        ];

        try
        {
            if ($tipo == "a")
            {
                DB::table('asistentes')
                    ->where('cons_folio', $folio)
                    ->update([
                        'cons_clicks' => $constancia->cons_clicks + 1
                    ]);
            }
            else if ($tipo == "p")
            {
                DB::table('ponentes')
                    ->where('cons_folio', $folio)
                    ->update([
                        'cons_clicks' => $constancia->cons_clicks + 1
                    ]);
            }

            return RESPONSE::download($file, $nombreArchivo2, $headers);
            //return RESPONSE::file($file, $nombreArchivo2, $headers);
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException $ex) {
            Log::info('Constancia no existente (download): ' . $tipo . "|" . $id . "|" . $folio . "-" . $ex->getMessage());
            return Redirect::to("https://www.kgrouptravel.com/error-constancia/");
        }
    }


    /**
     * Manda el e-mai para constancias
     * @param Request $request
     */
    public function mandarMailConstancias()
    {
        ini_set('max_execution_time', 300);
        $constancias_asi = DB::table('asistentes')->whereNotNull('cons_folio')->get();
        $constancias_pon = DB::table('ponentes')->whereNotNull('cons_folio')->get();

        //Envía e-mail de asistentes
        foreach ($constancias_asi as $constancia) {
            $datos['nombre_completo'] = trim($constancia->nombres2) . " " . trim($constancia->apellidos2);
            $datos['id_persona'] = $constancia->id;
            $datos['tipo_dato'] = 'a';
            $datos['cons_folio'] = $constancia->cons_folio;
            $datos['mail'] = $constancia->mail;

            Log::info('Envio constancia: ' . $constancia->id . " " . $constancia->mail);

            Mail::to($datos['mail'])
                ->cc('amiranda@endocrinologia.org.mx')
                ->bcc('df@kgroup.com.mx')
                //->cc('df@kgroup.com.mx')
                //->cc('galicia.mario@gmail.com')
                ->send(new ConstanciaGenerated($datos));

            sleep(2);

        }

        //Envía e-mail de asistentes
        foreach ($constancias_pon as $constancia) {
            $datos['nombre_completo'] = trim($constancia->nombres) . " " . trim($constancia->apellidos);
            $datos['id_persona'] = $constancia->id;
            $datos['tipo_dato'] = 'p';
            $datos['cons_folio'] = $constancia->cons_folio;
            $datos['mail'] = $constancia->mail;

            Log::info('Envio constancia: ' . $constancia->id . " " . $constancia->mail);

            Mail::to($datos['mail'])
                ->cc('amiranda@endocrinologia.org.mx')
                ->bcc('df@kgroup.com.mx')
                //->cc('df@kgroup.com.mx')
                //->cc('galicia.mario@gmail.com')
                ->send(new ConstanciaGenerated($datos));

            sleep(2);

        }

    }

}