<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Registros extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'registros';

    protected $fillable = [
        'clave_frm', 'fh_registro', 'form_id','submission_id','webhook_url',
        'ip', 'form_title','pretty','username','raw_request','type',
    ];
}
