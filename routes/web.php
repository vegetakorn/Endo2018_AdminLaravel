<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function () {
    return view('admin_template');
});
Route::post('logout', 'CustomAuthController@logout')->name('logout');
/*Route::get('login', function () {
    return view('login');
});*/

Route::get('test', 'TestController@index');

/**
 * Rutas para constancias
 */
//Route::get("constancia", "PdfController@index");
//Route::post("/genconsimg","PdfController@downloadPDFImg")->name('genpdfimg');
Route::get("/const/{tipo}/{id}/{folio}","PdfController@descargaConstancia")->name('descargaConst');
//Route::get("/mandaMails","PdfController@mandarMailConstancias");
Route::get("/crearconstancia/{folio}","PdfController@crarConstanciaId");

Route::get("cregister", "CustomAuthController@showRegisterForm")->name('cregister');
Route::post("cregister", "CustomAuthController@register");

Route::get("clogin", "CustomAuthController@showLoginForm")->name('clogin');
Route::post("clogin", "CustomAuthController@login");

//Route::resource('especialidades', 'EspecialidadController')->name('especialidades');
Route::get('especialidades', 'EspecialidadController@index')->name('especialidades');


//edición de la especialidad
Route::get('formespecialidad', 'EspecialidadController@create')->name('formespecialidad');
Route::post('formespecialidad', 'EspecialidadController@store');
Route::get('formespecialidad/{id}', 'EspecialidadController@edit')->where('id', '[0-9]+');

//modulo de socios
Route::get('socios/registros', 'AsistentesController@index')->name('socios.registros');
Route::post('socios/filtro', 'AsistentesController@filter')->name('socios.filtro');
Route::get('socios/info/{id}', 'AsistentesController@show')->name('socios.info')->where('id', '[0-9]+');
Route::get('socios/pago/{asistente}/{registro?}/{pago}', 'AsistentesController@showpago')->name('socios.pago')->where(['asistente' => '[0-9]+','registro' => '[0-9]+', 'pago' => '[0-9]+' ]);
//modulo de anualidades
Route::get('socios/anualidades/{id}', 'AsistentesController@showanualidad')->name('socios.anualidades')->where('id', '[0-9]+');;

//módulo de prueba de excel
Route::get('registros/exportar', 'ExportarController@generar')->name('registros.exportar');

